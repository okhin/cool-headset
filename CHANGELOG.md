# Changelog

All notable changes in the project will be noted here. Based on the
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/)format.

## Unreleased
### Added
* Sleep management
  * Will try to reconnect to known device on startup
  * Will be discoverable if it can't
  * Will go into deepsleep after 30s of being discoverable
  * Will go into deepsleep after push and release of play/pause button (button 1) if there's at least 4 seconds between the two edges
  * Will get out of deepsleep by a single push on button 1
  * Turns the screen off in deep sleep
* Displaying events
  * Using the u8g2 libs as a git-submodule
  * Three display mode :
    * One Line for one line message
    * Yes No for asking a simple question to a user
    * NOw PLAYing to display the current playing status
* Player event loop.
  * Volume change from BT controller
  * Play - Pause change from A2DP events
  * Play - Pause from GPIO buttons sent to target bluetooth
  * Prev - Next and Vol keys works
* GPIO post to Queue
* First signals for the BT stack (connected/disconnected)
* A2DP sink working
* I2S decoding via the internal GAP
* AVRC target volume notification
