/*
 * hal.h - copied from https://github.com/nkolban/esp32-snippets/blob/master/hardware/displays/U8G2/ 
 *
 *  Created on: Feb 12, 2017
 *      Author: kolban
 */

#ifndef U8G2_ESP32_HAL_H_
#define U8G2_ESP32_HAL_H_
#include "u8g2.h"

#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "display.h"

#define U8G2_ESP32_HAL_UNDEFINED (-1)

#define I2C_MASTER_NUM CONFIG_DISPLAY_I2C_PORT_NUM
#define I2C_MASTER_TX_BUF_DISABLE   0      //  I2C master do not need buffer
#define I2C_MASTER_RX_BUF_DISABLE   0      //  I2C master do not need buffer
#define I2C_MASTER_FREQ_HZ          CONFIG_DISPLAY_I2C_MASTER_FREQUENCY
#define I2C_SDA CONFIG_DISPLAY_I2C_SDA
#define I2C_SCL CONFIG_DISPLAY_I2C_SCL
#define I2C_ADDRESS (CONFIG_DISPLAY_I2C_ADDRESS << 1)
#define I2C_TIMEOUT_MS  100
#define ACK_CHECK_EN   0x1                 //  I2C master will check ack from slave
#define ACK_CHECK_DIS  0x0                 //  I2C master will not check ack from slave

typedef struct {
	gpio_num_t clk;
	gpio_num_t mosi;
	gpio_num_t sda; // data for I²C
	gpio_num_t scl; // clock for I²C
	gpio_num_t cs;
	gpio_num_t reset;
	gpio_num_t dc;
} u8g2_esp32_hal_t ;

#define U8G2_ESP32_HAL_DEFAULT {U8G2_ESP32_HAL_UNDEFINED, U8G2_ESP32_HAL_UNDEFINED, U8G2_ESP32_HAL_UNDEFINED, U8G2_ESP32_HAL_UNDEFINED, U8G2_ESP32_HAL_UNDEFINED, U8G2_ESP32_HAL_UNDEFINED, U8G2_ESP32_HAL_UNDEFINED }

uint8_t u8g2_esp32_i2c_byte_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);
uint8_t u8g2_esp32_gpio_and_delay_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);

i2c_config_t i2c_config;

#endif /* U8G2_ESP32_HAL_H_ */
