#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/ringbuf.h"
#include "esp_log.h"
#include "driver/i2s.h"

#include "player.h"
#include "audio.h"

#define AUDIO_TAG "AUDIO"

static RingbufHandle_t i2s_ringbuf = NULL;
static TaskHandle_t i2s_task_handle = NULL;

static void i2s_task_handler(void *arg)
{
    uint8_t *data = NULL;
    size_t len = 0;
    size_t bytes_w = 0;

    for(;;) {
        data = (uint8_t *)xRingbufferReceive(i2s_ringbuf, &len, portMAX_DELAY);
        if (len != 0) {
            ESP_ERROR_CHECK(i2s_write(I2S_PORT, (void*) data, len, &bytes_w, portMAX_DELAY));
            vRingbufferReturnItem(i2s_ringbuf, (void *) data);
        } else {
            ESP_LOGW(AUDIO_TAG, "I2S_TASK received 0 bytes");
        }
    }
}

size_t write_ringbuf(const uint8_t *data, size_t len)
{
    if (i2s_ringbuf) {
        uint8_t *volumed_data = (uint8_t *)malloc(sizeof(uint8_t) * len);
        BaseType_t done = xRingbufferSend(i2s_ringbuf, (void *)data, len, (TickType_t) 10);
        if (done) {
            free(volumed_data);
            return len;
        } else {
            ESP_LOGD(AUDIO_TAG, "Could not write to the ring buffer");
            free(volumed_data);
            return 0;
        }
    }
    ESP_LOGE(AUDIO_TAG, "i2s_ringbuf is NULL | no free space");
    return 0;
}

void i2s_init()
{
    i2s_config_t i2s_config = {

        .mode = I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_DAC_BUILT_IN,
        .bits_per_sample = 16,
        .sample_rate = 44100,
        .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
        .communication_format = I2S_COMM_FORMAT_STAND_I2S,
        .dma_buf_count = 6,
        .dma_buf_len = 64,
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
        .use_apll = true,
        .fixed_mclk = 44100 * 16 * 2,
        .tx_desc_auto_clear = true
    };

    ESP_ERROR_CHECK(i2s_driver_install(I2S_PORT, &i2s_config, 0, NULL));
    ESP_ERROR_CHECK(i2s_set_pin(I2S_PORT, NULL));
    ESP_ERROR_CHECK(i2s_set_dac_mode(I2S_DAC_CHANNEL_BOTH_EN));
}

void i2s_task_startup()
{
    i2s_ringbuf = xRingbufferCreate(1024 * 64, RINGBUF_TYPE_BYTEBUF);
    if (i2s_ringbuf == NULL) {
        ESP_LOGE(AUDIO_TAG, "Cannot create RingBuffer");
        abort();
    };

    ESP_ERROR_CHECK(i2s_zero_dma_buffer(I2S_PORT));
    xTaskCreatePinnedToCore(i2s_task_handler, "i2s_ringbuf_task", 4096, NULL, configMAX_PRIORITIES - 2, &i2s_task_handle, 1);
}

void i2s_task_shutdown()
{
    vTaskDelete(i2s_task_handle);
    vRingbufferDelete(i2s_ringbuf);
    ESP_ERROR_CHECK(i2s_zero_dma_buffer(I2S_PORT));
    player.state = PLAYER_STATE_STOP;
}
