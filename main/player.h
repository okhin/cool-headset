#ifndef __PLAYER_H
#define __PLAYER_H

#define VOL_INC 6
#define PLAYER_TAG "PLAYER"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event.h"
#include "esp_peripherals.h"
#include "audio_element.h"
#include "audio_error.h"
#include "audio_pipeline.h"

#define INPUT_GPIO_0 15
#define INPUT_GPIO_1 32
#define INPUT_GPIO_2 14

#define PREV_BUTTON INPUT_GPIO_0
#define PLAY_BUTTON INPUT_GPIO_1
#define NEXT_BUTTON INPUT_GPIO_2

#define INPUT_GPIO_PIN_MASK ((1ULL << INPUT_GPIO_0) | \
        (1ULL << INPUT_GPIO_1) | \
        (1ULL << INPUT_GPIO_2))

#define INPUT_NUM   3

#define INPUT_PRESS_TIME    CONFIG_PRESS_TIME

ESP_EVENT_DECLARE_BASE(PLAYER_EVENTS);

typedef enum player_state {
    PLAYER_STATE_PAUSE,
    PLAYER_STATE_STOP,
    PLAYER_STATE_PLAY,
    PLAYER_STATE_MENU
} player_state_t;

typedef struct s_player_yesno {
    void (*yes_cb)(void*);
    void (*no_cb)(void*);
    size_t cb_param_size;
    void *cb_param;
} s_player_yesno_t;

typedef struct s_player {
    uint8_t volume;
    player_state_t state;
    esp_periph_set_handle_t periph_set;
    audio_event_iface_handle_t evt;
    audio_pipeline_handle_t pipeline;
    char artist[256];
    char album[256];
    char track[256];
    s_player_yesno_t menu;
} s_player_t;

s_player_t player;
esp_event_loop_handle_t player_event_loop;

void player_task(void* pvParameters);
void player_init();

enum {
    // Those _SEND events are the one we emit and send to the BT hosts
    PLAYER_EVENT_TOGGLE_SEND,
    PLAYER_EVENT_NEXT_SEND,
    PLAYER_EVENT_PREV_SEND,
    PLAYER_EVENT_VOLUP_SEND,
    PLAYER_EVENT_VOLDN_SEND,
    
    // Those _RECEIVE events are the one sent to us by the BT hosts
    PLAYER_EVENT_TOGGLE_RECEIVE,
    PLAYER_EVENT_VOL_CHANGE_RECEIVE,

    // Power event
    PLAYER_EVENT_DEEPSLEEP,
    PLAYER_EVENT_LIGHTSLEEP,

    // Display event
    PLAYER_EVENT_MESSAGE,
};

#endif
