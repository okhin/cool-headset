#ifndef __AUDIO_H
#define __AUDIO_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/ringbuf.h"
#include "driver/i2s.h"

#define I2S_PORT 0

void i2s_init();
void i2s_task_startup();
void i2s_task_shutdown();
size_t write_ringbuf(const uint8_t *data, size_t size);

#endif // __AUDIO_H
