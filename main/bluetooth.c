#include <string.h>
#include <esp_gap_bt_api.h>
#include <esp_a2dp_api.h>
#include <esp_avrc_api.h>

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "freertos/timers.h"

#include "nvs.h"
#include "nvs_flash.h"
#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include "esp_log.h"
#include "esp_event.h"
#include "esp_sleep.h"

#include "driver/i2s.h"

// Application headers
#include "bluetooth.h"
#include "player.h"
#include "display.h"

const char *s_a2d_audio_state_str[] = {"Paused", "Stopped", "Playing"};
const char *s_a2d_conn_state_str[] = {"Disconnected", "Connecting", "Connected", "Disconnecting"};

static xTimerHandle bt_sleep_timer = NULL;

static esp_avrc_rn_evt_cap_mask_t avrc_peer_rn_cap;
static esp_bd_addr_t * bt_bonded_devices = NULL;

// Check the SSP key
static void bt_gap_auth_valid(esp_bt_gap_cb_param_t *param)
{
    ESP_ERROR_CHECK(esp_bt_gap_ssp_confirm_reply(param->cfm_req.bda, true));
    ESP_LOGI(BT_TAG, "SSP Confirm is valid.");
    dpl_msg_t *dpl_msg = malloc(sizeof(dpl_msg_t));
    dpl_msg->id = DISPLAY_ONELINE;
    dpl_msg->params.one_line.str = "Auth successful";
    player.state = PLAYER_STATE_STOP;

    esp_periph_handle_t display_handle = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_CONSOLE);
    ESP_ERROR_CHECK(esp_periph_send_cmd(display_handle, PLAYER_EVENT_MESSAGE,
                dpl_msg, sizeof(dpl_msg_t)));
}

static void bt_gap_auth_unvalid(esp_bt_gap_cb_param_t *param)
{
    ESP_ERROR_CHECK(esp_bt_gap_ssp_confirm_reply(param->cfm_req.bda, false));
    ESP_LOGW(BT_TAG, "SSP Confirm is invalid.");
    dpl_msg_t *dpl_msg = malloc(sizeof(dpl_msg_t));
    dpl_msg->id = DISPLAY_ONELINE;
    dpl_msg->params.one_line.str = "Auth failure";
    player.state = PLAYER_STATE_STOP;

    esp_periph_handle_t display_handle = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_CONSOLE);
    ESP_ERROR_CHECK(esp_periph_send_cmd(display_handle, PLAYER_EVENT_MESSAGE,
                dpl_msg, sizeof(dpl_msg_t)));
}

static void bt_new_track()
{
    // Request metadata
    uint8_t attr_mask = ESP_AVRC_MD_ATTR_TITLE | ESP_AVRC_MD_ATTR_ARTIST | ESP_AVRC_MD_ATTR_ALBUM;
    ESP_ERROR_CHECK(esp_avrc_ct_send_metadata_cmd(esp_random() % 16, attr_mask));

    // If the peer support notification, let's notify it
    if (esp_avrc_rn_evt_bit_mask_operation(ESP_AVRC_BIT_MASK_OP_TEST,
                &avrc_peer_rn_cap, ESP_AVRC_RN_TRACK_CHANGE)) {
        ESP_ERROR_CHECK(esp_avrc_ct_send_register_notification_cmd(esp_random() % 16,
                    ESP_AVRC_RN_TRACK_CHANGE, 0));
    }
}

// AVRC notification handler
static void bt_avrc_notify_handler(uint8_t event, esp_avrc_rn_param_t *event_parameter)
{
    switch(event) {
        case ESP_AVRC_RN_TRACK_CHANGE:
            ESP_LOGD(BT_TAG, "ESP_AVRC_RN_TRACK_CHANGE");
            bt_new_track();
            break;
        case ESP_AVRC_RN_PLAY_STATUS_CHANGE:
        case ESP_AVRC_RN_PLAY_POS_CHANGED:
        case ESP_AVRC_RN_VOLUME_CHANGE:
        default:
            ESP_LOGW(BT_TAG, "AVRC notification unhandled: %d", event);
            break;
    }
}

// AVRC TG callback
static void bt_avrc_tg_cb(esp_avrc_tg_cb_event_t event, esp_avrc_tg_cb_param_t *param)
{
    switch(event) {
        case ESP_AVRC_TG_SET_ABSOLUTE_VOLUME_CMD_EVT:
            ESP_LOGD(BT_TAG, "ESP_AVRC_TG_SET_ABSOLUTE_VOLUME_CMD_EVT: volume %d%%",
                    param->set_abs_vol.volume * 100 / 0x7f);
            player.volume = param->set_abs_vol.volume;
            ESP_ERROR_CHECK(BT_EVENT(bt_service.periph, BT_AVRC_TG_VOLUME_CHANGED));
            break;
        case ESP_AVRC_TG_REGISTER_NOTIFICATION_EVT: {
                ESP_LOGD(BT_TAG, "ESP_AVRC_TG_REGISTER_NOTIFICATION_EVT");
                esp_avrc_rn_param_t rn_param;
                switch(param->reg_ntf.event_id) {
                    case ESP_AVRC_RN_VOLUME_CHANGE:
                        ESP_LOGD(BT_TAG, "ESP_AVRC_RN_VOLUME_CHANGE");
                        rn_param.volume = player.volume;
                        ESP_ERROR_CHECK(esp_avrc_tg_send_rn_rsp(ESP_AVRC_RN_VOLUME_CHANGE,
                                    ESP_AVRC_RN_RSP_INTERIM, &rn_param));
                        break;
                    default:
                        ESP_LOGW(BT_TAG, "ESP_AVRC_TG: Unhandled notification %d", param->reg_ntf.event_id);
                        break;
                }
                break;
            }
        case ESP_AVRC_TG_CONNECTION_STATE_EVT:
            ESP_LOGD(BT_TAG, "ESP_AVRC_TG_CONNECTION_STATE_EVT");
            if (param->conn_stat.connected) {
                ESP_LOGD(BT_TAG, "AVRC Target connected");
            } else {
                ESP_LOGW(BT_TAG, "AVRC Target disconnected");
            }
            break;
        case ESP_AVRC_TG_REMOTE_FEATURES_EVT:
        case ESP_AVRC_TG_PASSTHROUGH_CMD_EVT:
        default:
            ESP_LOGW(BT_TAG, "ESP_AVRC_TG: Unhandled event %d", event);
            break;
    }
}

// AVRC CT handling
static void bt_avrc_ct_cb(esp_avrc_ct_cb_event_t event, esp_avrc_ct_cb_param_t *param)
{
    switch(event) {
        case ESP_AVRC_CT_CONNECTION_STATE_EVT:
            ESP_LOGD(BT_TAG, "ESP_AVRC_CT_CONNECTION_STATE_EVT");
            if (param->conn_stat.connected) {
                esp_avrc_ct_send_get_rn_capabilities_cmd(esp_random() % 16);
            } else {
                avrc_peer_rn_cap.bits = 0;
            }
            break;
#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 0, 0))
        case ESP_AVRC_CT_GET_RN_CAPABILITIES_RSP_EVT:
            ESP_LOGD(BT_TAG, "ESP_AVRC_CT_GET_RN_CAPABILITIES_RSP_EVT");
            avrc_peer_rn_cap.bits = param->get_rn_caps_rsp.evt_set.bits;
            bt_new_track();
            break;
#endif
        case ESP_AVRC_CT_CHANGE_NOTIFY_EVT:
            ESP_LOGD(BT_TAG, "ESP_AVRC_CT_CHANGE_NOTIFY_EVT");
            bt_avrc_notify_handler(param->change_ntf.event_id,
                    &param->change_ntf.event_parameter);
            break;
        case ESP_AVRC_CT_METADATA_RSP_EVT:
            ESP_LOGD(BT_TAG, "ESP_AVRC_CT_METADATA_RSP_EVT");
            switch(param->meta_rsp.attr_id) {
                case ESP_AVRC_MD_ATTR_TITLE:
                    snprintf(player.track, param->meta_rsp.attr_length + 1,
                            "%s", param->meta_rsp.attr_text);
                    break;
                case ESP_AVRC_MD_ATTR_ARTIST:
                    snprintf(player.artist, param->meta_rsp.attr_length + 1,
                            "%s", param->meta_rsp.attr_text);
                    break;
                case ESP_AVRC_MD_ATTR_ALBUM:
                    snprintf(player.album, param->meta_rsp.attr_length + 1,
                            "%s", param->meta_rsp.attr_text);
                    break;
            }
            dpl_msg_t *dpl_msg = malloc(sizeof(dpl_msg_t));
            dpl_msg->id = DISPLAY_NOPLAY;
            esp_periph_handle_t display_handle = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_CONSOLE);
            ESP_ERROR_CHECK(esp_periph_send_event(display_handle, PLAYER_EVENT_MESSAGE,
                        dpl_msg, sizeof(dpl_msg_t)));
            break;
        case ESP_AVRC_CT_PASSTHROUGH_RSP_EVT:
        case ESP_AVRC_CT_PLAY_STATUS_RSP_EVT:
        case ESP_AVRC_CT_REMOTE_FEATURES_EVT:
        case ESP_AVRC_CT_SET_ABSOLUTE_VOLUME_RSP_EVT:
        default:
            ESP_LOGW(BT_TAG, "ESP_AVRC_CT Unhandled event received: %d", event);
            break;
    }
}

// Callback to work dispatcher
static void bt_a2d_cb(esp_a2d_cb_event_t event, esp_a2d_cb_param_t *param)
{
    esp_periph_handle_t self = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_BLUETOOTH);
    ESP_LOGI(BT_TAG, "A2D WORK event - %d", event);
    switch(event) {
        case ESP_A2D_CONNECTION_STATE_EVT:
            ESP_LOGI(BT_TAG, "ESP_A2D_CONNECTION_STATE_EVT %s",
                    s_a2d_conn_state_str[param->conn_stat.state]);
            // We want to send the disconnect signal only if we were connected
            if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_DISCONNECTED &&
                    bt_service.state == BT_STATE_CONNECTED) {
                BT_SIGNAL(self, BT_SIGNAL_DISCONNECTED);
            } else if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_CONNECTED) {
                BT_SIGNAL(self, BT_SIGNAL_CONNECTED);
            };
            break;
        case ESP_A2D_AUDIO_STATE_EVT:
            ESP_LOGD(BT_TAG, "ESP_A2D_AUDIO_STATE_EVT %d", param->audio_stat.state);
            switch(param->audio_stat.state) {
                case ESP_A2D_AUDIO_STATE_STARTED:
                    BT_EVENT(self, BT_SIGNAL_AUDIO_STARTED);
                    break;
                case ESP_A2D_AUDIO_STATE_REMOTE_SUSPEND:
                    BT_EVENT(self, BT_SIGNAL_AUDIO_SUSPENDED);
                    break;
                case ESP_A2D_AUDIO_STATE_STOPPED:
                    BT_EVENT(self, BT_SIGNAL_AUDIO_STOPPED);
                    break;
                default:
                    ESP_LOGW(BT_TAG, "ESP_A2D_AUDIO_STATE_EVT %d unsupported", param->audio_stat.state);
                    break;
            }

            break;
        case ESP_A2D_AUDIO_CFG_EVT:
            ESP_LOGD(BT_TAG, "ESP_A2D_AUDIO_CFG_EVT, codec type: %d", param->audio_cfg.mcc.type);
            // For now, we're only using SBC
            switch(param->audio_cfg.mcc.type) {
                case ESP_A2D_MCT_SBC:
                    {
                        int sample_rate = 16000;
                        char oct0 = param->audio_cfg.mcc.cie.sbc[0];
                        if (oct0 & (0x01 << 6)) {
                            sample_rate = 32000;
                        } else if (oct0 & (0x01 << 5)) {
                            sample_rate = 44100;
                        } else if (oct0 & (0x01 << 4)) {
                            sample_rate = 48000;
                        };

                        audio_element_set_music_info(bt_service.stream, sample_rate, 2, 16);
                        audio_element_report_info(bt_service.stream);
                        ESP_LOGI(BT_TAG, "Audio samplerate configured: %d", sample_rate);
                        break;
                    }
                default:
                    ESP_LOGW(BT_TAG, "Unsupported MCC codec: %d", param->audio_cfg.mcc.type);
                    break;
            }
            break;
        case ESP_A2D_PROF_STATE_EVT:
        case ESP_A2D_MEDIA_CTRL_ACK_EVT:
        default:
            ESP_LOGW(BT_TAG, "BT_A2D Unhandled event received: %d", event);
            break;
    }
}

static void bt_a2d_data_cb(const uint8_t *data, uint32_t len)
{
    ESP_LOGV(BT_TAG, "A2d data received %d bytes", len);
    if (bt_service.stream) {
        if (audio_element_get_state(bt_service.stream) == AEL_STATE_RUNNING)
            audio_element_output(bt_service.stream, (char *)data, len);
    }
}

static void bt_sleep_timer_expired(TimerHandle_t timer)
{
    esp_periph_handle_t self = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_BLUETOOTH);
    BT_SIGNAL(self, BT_SIGNAL_DEEPSLEEP);
}

static void bt_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param)
{
    switch(event) {
        case ESP_BT_GAP_AUTH_CMPL_EVT:
            ESP_LOGD(BT_TAG, "ESP_BT_GAP_AUTH_CMPL_EVT");
            if (param->auth_cmpl.stat == ESP_BT_STATUS_SUCCESS) {
                ESP_LOGI(BT_TAG, "Authentication successfull and complete");
                bt_service.state = BT_STATE_CONNECTED;
            } else {
                ESP_LOGE(BT_TAG, "Authentication failed. Status : %d", param->auth_cmpl.stat);
            }
            break;
#if (CONFIG_BT_SSP_ENABLED == true)
        case ESP_BT_GAP_CFM_REQ_EVT: {
            ESP_LOGD(BT_TAG, "ESP_BT_GAP_CFM_REQ_EVT");
            ESP_LOGI(BT_TAG, "Auth required, please compare the value: %06d", param->cfm_req.num_val);
            dpl_msg_t *dpl_msg = malloc(sizeof(dpl_msg_t));
            s_player_yesno_t player_yesno;

            dpl_msg->id = DISPLAY_YESNO;
            dpl_msg->params.yes_no.str[0] = "Check this code";
            dpl_msg->params.yes_no.str[1] = malloc(7*sizeof(char));
            sprintf(dpl_msg->params.yes_no.str[1], "%06d", param->cfm_req.num_val);
            player.menu.yes_cb = &bt_gap_auth_valid;
            player.menu.no_cb = &bt_gap_auth_unvalid;
            player.menu.cb_param_size = sizeof(*param);
            player.menu.cb_param = malloc(player.menu.cb_param_size);
            memcpy(player.menu.cb_param, param, sizeof(*param));

            ESP_ERROR_CHECK(esp_periph_send_cmd(esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_CONSOLE), PLAYER_EVENT_MESSAGE, dpl_msg, sizeof(dpl_msg_t)));
            break;
        }
        case ESP_BT_GAP_KEY_NOTIF_EVT:
            ESP_LOGD(BT_TAG, "ESP_BT_GAP_KEY_NOTIF_EVT");
            ESP_LOGI(BT_TAG, "Passkey: %d", param->key_notif.passkey);
            break;
        case ESP_BT_GAP_KEY_REQ_EVT:
            ESP_LOGD(BT_TAG, "ESP_BT_GAP_KEY_REQ_EVT");
            break;
#endif
        case ESP_BT_GAP_DISC_RES_EVT: // 0
        case ESP_BT_GAP_DISC_STATE_CHANGED_EVT: //1
        case ESP_BT_GAP_RMT_SRVCS_EVT: // 2
        case ESP_BT_GAP_RMT_SRVC_REC_EVT: // 3
        case ESP_BT_GAP_PIN_REQ_EVT: // 5
        case ESP_BT_GAP_READ_RSSI_DELTA_EVT: // 9
        case ESP_BT_GAP_CONFIG_EIR_DATA_EVT: // 10
        case ESP_BT_GAP_SET_AFH_CHANNELS_EVT: // 11
        case ESP_BT_GAP_READ_REMOTE_NAME_EVT: // 12
        default:
            ESP_LOGW(BT_TAG, "Unhandled GAP event received: %d", event);
            break;
    }
}

static void bt_init()
{

    // Let's config the security for pairing
#if (CONFIG_BT_SSP_ENABLED == true)
    // We have SSP
    esp_bt_sp_param_t param_type = ESP_BT_SP_IOCAP_MODE;
    esp_bt_io_cap_t iocap = ESP_BT_IO_CAP_IO;
    ESP_ERROR_CHECK(esp_bt_gap_set_security_param(param_type, &iocap, sizeof(uint8_t)));

#endif
    // Legacy auth
    esp_bt_pin_type_t pin_type = ESP_BT_PIN_TYPE_FIXED;
    esp_bt_pin_code_t pin_code = {1, 2, 3, 4};

    ESP_ERROR_CHECK(esp_bt_gap_set_pin(pin_type, 4, pin_code));

    // Let's get the GAP stack in order and start needed profiles
    ESP_ERROR_CHECK(esp_bt_dev_set_device_name(CONFIG_BLUETOOTH_DEVICE_NAME));
    ESP_ERROR_CHECK(esp_bt_gap_register_callback(bt_gap_cb));

    // Fire up AVRC target
    ESP_ERROR_CHECK(esp_avrc_tg_init());
    ESP_ERROR_CHECK(esp_avrc_tg_register_callback(bt_avrc_tg_cb));
    // And let's register volume change
    esp_avrc_rn_evt_cap_mask_t evt_set = {0};
    esp_avrc_rn_evt_bit_mask_operation(ESP_AVRC_BIT_MASK_OP_SET,
                &evt_set, ESP_AVRC_RN_VOLUME_CHANGE);
    ESP_ERROR_CHECK(esp_avrc_tg_set_rn_evt_cap(&evt_set));

    // Fire up AVRC controller
    ESP_ERROR_CHECK(esp_avrc_ct_init());
    ESP_ERROR_CHECK(esp_avrc_ct_register_callback(bt_avrc_ct_cb));
    
    // Prepare the callbacks for AD2P
    ESP_ERROR_CHECK(esp_a2d_sink_init());
    ESP_ERROR_CHECK(esp_a2d_register_callback(bt_a2d_cb));
    ESP_ERROR_CHECK(esp_a2d_sink_register_data_callback(bt_a2d_data_cb));

    // Expose ourselves to connection and start scanning
    ESP_ERROR_CHECK(esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE, ESP_BT_GENERAL_DISCOVERABLE));

    bt_service.state = BT_STATE_DISCOVERABLE;
}

esp_err_t _bt_periph_run(esp_periph_handle_t self, audio_event_iface_msg_t *msg)
{
    int ret = ESP_OK;
    ESP_LOGI(BT_TAG, "Signal received : %d", msg->cmd);
    dpl_msg_t *dpl_msg = malloc(sizeof(dpl_msg_t));
    // Let's get some peripherals we might need
    esp_periph_handle_t display_handle = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_CONSOLE);
    switch(msg->cmd) {
        case BT_SIGNAL_INIT:
            ESP_LOGI(BT_TAG, "BT_SIGNAL_INIT");
            bt_init();
            break;

        case BT_SIGNAL_DISCONNECTED:
            ESP_LOGI(BT_TAG, "BT_SIGNAL_DISCONNECTED");
            ESP_ERROR_CHECK(esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE,
                        ESP_BT_GENERAL_DISCOVERABLE));
            dpl_msg->id = DISPLAY_ONELINE;
            dpl_msg->params.one_line.str = CONFIG_BLUETOOTH_DEVICE_NAME;
            ESP_ERROR_CHECK(esp_periph_send_cmd(display_handle, PLAYER_EVENT_MESSAGE,
                        dpl_msg, sizeof(dpl_msg_t)));

            ESP_ERROR_CHECK(esp_avrc_ct_deinit());
            ESP_ERROR_CHECK(esp_avrc_tg_deinit());
            ESP_ERROR_CHECK(esp_a2d_sink_deinit());
            ESP_ERROR_CHECK(esp_avrc_ct_init());
            ESP_ERROR_CHECK(esp_avrc_tg_init());
            ESP_ERROR_CHECK(esp_a2d_sink_init());
            break;

        case BT_SIGNAL_CONNECTED:
            ESP_LOGI(BT_TAG, "BT_SIGNAL_CONNECTED");
            bt_service.state = BT_STATE_CONNECTED;
            // We can now kill the sleep timer since we're connected
            if (bt_sleep_timer) {
                xTimerDelete(bt_sleep_timer, 0);
            }

            ESP_ERROR_CHECK(esp_bt_gap_set_scan_mode(ESP_BT_NON_CONNECTABLE,
                        ESP_BT_NON_DISCOVERABLE));
            ESP_ERROR_CHECK(esp_bt_gap_cancel_discovery());

            // We need to add the stream to the pipeline
            ESP_ERROR_CHECK(audio_pipeline_register(player.pipeline, bt_service.stream, "a2d_sink"));
            const char * pipeline_link[] = {"a2d_sink", "i2s_writer"};
            ESP_ERROR_CHECK(audio_pipeline_link(player.pipeline, pipeline_link, 2));
            ESP_ERROR_CHECK(audio_pipeline_run(player.pipeline));

            player.state = PLAYER_STATE_STOP;
            // We want to ask for the Metadata
            bt_new_track();
            break;

        case BT_SIGNAL_DEEPSLEEP:
            ESP_LOGI(BT_TAG, "BT_SIGNAL_DEEPSLEEP");
            ESP_LOGD(BT_TAG, "Shutting down screen");
            display_powersave(1);
            if (bt_service.state == BT_STATE_CONNECTED) {
                ESP_LOGD(BT_TAG, "Disabling i2s");
            //    i2s_task_shutdown();
            }
            //ESP_LOGD(BT_TAG, "Disabling the BT controller");
            //ESP_ERROR_CHECK(esp_bt_controller_disable());
            ESP_LOGD(BT_TAG, "Entering deepsleep");
            //esp_deep_sleep_start();
            break;

        default:
            ESP_LOGW(BT_TAG, "Unhandled signal: %d", msg->cmd);
            break;
    }
    if (msg->need_free_data)
        free(msg->data);
    return ret;
}

esp_err_t _bt_periph_destroy(esp_periph_handle_t self)
{
    // Let's deinit the BT stack
    esp_bt_controller_disable();
    esp_bt_controller_deinit();

    return ESP_OK;
}

esp_err_t _bt_periph_init(esp_periph_handle_t self)
{
    // Initializing the nvs flash
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // We need to reinit the NVS
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    };
    ESP_ERROR_CHECK(err);

     // No need for BLE for now, let's free the heap of it
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_BLE));

    // Let's init the controller and the BT stack
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    bt_cfg.mode = ESP_BT_MODE_CLASSIC_BT;
    ESP_ERROR_CHECK(esp_bt_controller_init(&bt_cfg));
    ESP_ERROR_CHECK(esp_bt_controller_enable(ESP_BT_MODE_CLASSIC_BT));
    ESP_ERROR_CHECK(esp_bluedroid_init());
    ESP_ERROR_CHECK(esp_bluedroid_enable());

    // We need to define a class of device of sort
    esp_bt_cod_t bt_cod = {
        .major = BT_MAJOR_CLASS,
        .minor = BT_MINOR_CLASS,
        .service = BT_SERVICE_CLASS
    };
    ESP_ERROR_CHECK(esp_bt_gap_set_cod(bt_cod, ESP_BT_INIT_COD));

    bt_service.state = BT_STATE_STARTED;
    return ESP_OK;
}

esp_err_t bluetooth_periph_init(s_bt_service_t *service)
{
    AUDIO_NULL_CHECK(BT_TAG, service, return ESP_FAIL);
    ESP_LOGD(BT_TAG, "Initializing BT peripheral");
    if (service && service->periph)
    {
        ESP_LOGE(BT_TAG, "BT peripherals have already been created");
        return ESP_FAIL;
    }

    service->periph = esp_periph_create(PERIPH_ID_BLUETOOTH, "periph_bt");
    AUDIO_MEM_CHECK(BT_TAG, service->periph, return ESP_FAIL);
    esp_periph_set_function(service->periph, _bt_periph_init, _bt_periph_run, _bt_periph_destroy);
    return ESP_OK;
}

esp_err_t bluetooth_stream_init(s_bt_service_t *service)
{
    AUDIO_NULL_CHECK(BT_TAG, service, return ESP_FAIL);
    ESP_LOGD(BT_TAG, "Initialising BT stream");
    if (service && service->stream)
    {
        ESP_LOGE(BT_TAG, "BT stream have already been created");
        return ESP_FAIL;
    }

    audio_element_cfg_t cfg = DEFAULT_AUDIO_ELEMENT_CONFIG();
    cfg.task_stack = -1;
    cfg.tag = "bt";

    service->stream = audio_element_init(&cfg);
    AUDIO_MEM_CHECK(BT_TAG, service->stream, return ESP_FAIL);

    return ESP_OK;
}

esp_err_t bluetooth_service_init(s_bt_service_t *service)
{
    ESP_LOGD(BT_TAG, "Initialising BT services");
    ESP_LOGV(BT_TAG, "BT Service creating BT peripheral");
    service->state = BT_STATE_SHUTDOWN;
    ESP_ERROR_CHECK(bluetooth_periph_init(service));
    ESP_ERROR_CHECK(bluetooth_stream_init(service));
    return ESP_OK;
}
