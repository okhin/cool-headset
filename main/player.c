#include <string.h>

#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_avrc_api.h"
#include "esp_peripherals.h"
#include "periph_button.h"

#include "audio_element.h"

#include "i2s_stream.h"
#include "player.h"
#include "bluetooth.h"
#include "display.h"

#define I2S_PORT 0

static void player_volume_change(void * handler_args, esp_event_base_t base, int32_t id, void *event_data)
{
    uint8_t volume = *((uint8_t *)event_data);
    player.volume = volume;
}

static void player_send_volume(void * handler_args, esp_event_base_t base, int32_t id, void *event_data)
{
    uint8_t volume_target;
    if (id == PLAYER_EVENT_VOLUP_SEND) {
        if (player.volume > 121) {
            player.volume = 127;
        } else {
            player.volume += 6;
        }
    } else {
        if (player.volume < 6) {
            player.volume = 0;
        } else {
            player.volume -= 6;
        }
    }
    ESP_LOGD(PLAYER_TAG, "Set volume to %d", player.volume);
    
    esp_avrc_rn_param_t rn_param;
    rn_param.volume = player.volume;
    esp_avrc_tg_send_rn_rsp(ESP_AVRC_RN_VOLUME_CHANGE, ESP_AVRC_RN_RSP_CHANGED, &rn_param);
}

static void player_toggle(void *handler_args, esp_event_base_t base, int32_t id, void *event_data)
{
    player.state = (player.state == PLAYER_STATE_PLAY) ? PLAYER_STATE_PAUSE : PLAYER_STATE_PLAY;
}

static void player_send_passthrough(void *handler_args, esp_event_base_t base, int32_t event, void *event_data)
{
    esp_avrc_pt_cmd_t pt_cmd;
    switch(event) {
        case PLAYER_EVENT_TOGGLE_SEND:
            if (player.state == PLAYER_STATE_PLAY)
                pt_cmd = ESP_AVRC_PT_CMD_PAUSE;
            else
                pt_cmd = ESP_AVRC_PT_CMD_PLAY;
            break;
        case PLAYER_EVENT_NEXT_SEND:
            pt_cmd = ESP_AVRC_PT_CMD_FORWARD;
            break;
        case PLAYER_EVENT_PREV_SEND:
            pt_cmd = ESP_AVRC_PT_CMD_BACKWARD;
            break;
        default:
            return;
    }
    ESP_ERROR_CHECK(esp_avrc_ct_send_passthrough_cmd(
                esp_random() % 16, pt_cmd, ESP_AVRC_PT_CMD_STATE_PRESSED));
}

static void player_send_track_change(void * handler_args, esp_event_base_t base, int32_t id, void * event_data)
{
    if (id == PLAYER_EVENT_PREV_SEND) {
        ESP_ERROR_CHECK(esp_avrc_ct_send_passthrough_cmd(esp_random() % 16,
            ESP_AVRC_PT_CMD_BACKWARD, ESP_AVRC_PT_CMD_STATE_PRESSED));
    } else {
        ESP_ERROR_CHECK(esp_avrc_ct_send_passthrough_cmd(esp_random() % 16,
            ESP_AVRC_PT_CMD_FORWARD, ESP_AVRC_PT_CMD_STATE_PRESSED));
    }
}

static void player_sleep(void * handler_args, esp_event_base_t base, int32_t id, void * event_data)
{
    esp_periph_handle_t bt_handle = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_BLUETOOTH);
    BT_SIGNAL(bt_handle, BT_SIGNAL_DEEPSLEEP);
}

static void player_menu_handle(int button)
{
    ESP_LOGD(PLAYER_TAG, "YES/NO Menu choice made");
    switch(button) {
        case PREV_BUTTON:
            ESP_LOGI(PLAYER_TAG, "Yes pressed");
            ESP_LOGI(PLAYER_TAG, "yes_cb: %p", player.menu.yes_cb);
            (player.menu.yes_cb)(player.menu.cb_param);
            free(player.menu.cb_param);
            break;
        case NEXT_BUTTON:
            ESP_LOGI(PLAYER_TAG, "No pressed");
            ESP_LOGI(PLAYER_TAG, "no_cb: %p", player.menu.no_cb);
            (player.menu.no_cb)(player.menu.cb_param);
            free(player.menu.cb_param);
            break;
        default:
            ESP_LOGW(PLAYER_TAG, "Unsupported key %d", button);
            break;
    }
}

esp_err_t player_event_handle(audio_event_iface_msg_t *event, void *context)
{
    dpl_msg_t *dpl_msg = malloc(sizeof(dpl_msg_t));
    esp_periph_handle_t display_handle = esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_CONSOLE);

    audio_element_handle_t i2s_stream = audio_pipeline_get_el_by_tag(player.pipeline, "i2s_writer");
    ESP_LOGI(PLAYER_TAG, "Event received. Source: %d, event_id: %d", (int)event->source_type, event->cmd);
    switch((int)event->source_type) {
        case PERIPH_ID_BUTTON:
            ESP_LOGD(PLAYER_TAG, "Button[%d], event->event_id=%d", (int)event->data, event->cmd);
            if ((player.state == PLAYER_STATE_MENU))// && (event->cmd == 1))
            {
                ESP_LOGV(PLAYER_TAG, "Menu button pressed");
                player_menu_handle((int)event->data);
            }
            break;
        case PERIPH_ID_BLUETOOTH:
            ESP_LOGD(PLAYER_TAG, "Bluetooth event %d received", event->cmd);
            switch (event->cmd) {
                case BT_SIGNAL_AUDIO_STARTED:
                    ESP_LOGD(PLAYER_TAG, "BT_SIGNAL_AUDIO_STARTED");
                    player.state = PLAYER_STATE_PLAY;
                    ESP_ERROR_CHECK(i2s_alc_volume_set(i2s_stream, player.volume - 63));
                    ESP_ERROR_CHECK(audio_pipeline_resume(player.pipeline));
                    dpl_msg->id = DISPLAY_NOPLAY;
                    ESP_ERROR_CHECK(esp_periph_send_event(display_handle, PLAYER_EVENT_MESSAGE,
                                dpl_msg, sizeof(dpl_msg_t)));
                    break;

                case BT_SIGNAL_AUDIO_SUSPENDED:
                    ESP_LOGD(PLAYER_TAG, "BT_SIGNAL_AUDIO_SUSPENDED");
                    player.state = PLAYER_STATE_PAUSE;
                    ESP_ERROR_CHECK(audio_pipeline_pause(player.pipeline));
                    dpl_msg->id = DISPLAY_NOPLAY;
                    ESP_ERROR_CHECK(esp_periph_send_event(display_handle, PLAYER_EVENT_MESSAGE,
                                dpl_msg, sizeof(dpl_msg_t)));
                    break;

                case BT_SIGNAL_AUDIO_STOPPED:
                    ESP_LOGD(PLAYER_TAG, "BT_SIGNAL_AUDIO_STOPPED");
                    player.state = PLAYER_STATE_STOP;
                    ESP_ERROR_CHECK(audio_pipeline_stop(player.pipeline));
                    dpl_msg->id = DISPLAY_NOPLAY;
                    ESP_ERROR_CHECK(esp_periph_send_event(display_handle, PLAYER_EVENT_MESSAGE,
                                dpl_msg, sizeof(dpl_msg_t)));
                    break;

                case BT_AVRC_TG_VOLUME_CHANGED:
                    ESP_LOGD(PLAYER_TAG, "BT_AVRC_TG_VOLUME_CHANGED to %d (%d%%)",
                            player.volume, player.volume * 100 / 0x7f);
                    ESP_ERROR_CHECK(i2s_alc_volume_set(i2s_stream, player.volume - 63));

                    break;

                default:
                    ESP_LOGW(PLAYER_TAG, "Unknown event for PERIPH_ID_BLUETOOTH %d", event->cmd);
                    break;
            };
            break;
        case AUDIO_ELEMENT_TYPE_ELEMENT:
            ESP_LOGD(PLAYER_TAG, "AUDIO_ELEMENT_TYPE_ELEMENT message`");
            switch (event->cmd) {
                case AEL_MSG_CMD_REPORT_MUSIC_INFO:
                    if (event->source == (void *)(&(bt_service.stream))) {
                        audio_element_info_t music_info = {0};
                        audio_element_getinfo(bt_service.stream, &music_info);
                        ESP_LOGD(PLAYER_TAG, "Report music info from BT samples=%d, bits=%d, ch=%d",
                                music_info.sample_rates,
                                music_info.bits,
                                music_info.channels);

                        audio_element_setinfo(i2s_stream, &music_info);
                        ESP_ERROR_CHECK(i2s_stream_set_clk(i2s_stream, music_info.sample_rates, music_info.bits, music_info.channels));
                    }
                    break;
                default:
                    ESP_LOGW(PLAYER_TAG, "AUDIO_ELEMENT_TYPE_ELEMENT unhandled cmd %d", event->cmd);
                    break;
            }
            break;
        default:
            // Unsourced event
            ESP_LOGI(PLAYER_TAG, "Unsourced event");
            switch (event->cmd) {
                case PLAYER_EVENT_MESSAGE:
                    ESP_LOGD(PLAYER_TAG, "PLAYER_EVENT_MESSAGE");
                    ESP_ERROR_CHECK(esp_periph_send_cmd(esp_periph_set_get_by_id(player.periph_set, PERIPH_ID_CONSOLE), event->cmd, event->data, event->data_len));
                    break;
                default:
                    ESP_LOGW(PLAYER_TAG, "Unhandled event");
                    break;
            }
            break;
    }

    return ESP_OK;
}

void player_task(void* pvParameters)
{
    for (;;) {
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(player.evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK) {
            continue;
        }

        player_event_handle(&msg, NULL);

        if (msg.need_free_data)
            free(msg.data);
    }
}

void player_init()
{
    // Let's initialize some status
    ESP_LOGD(PLAYER_TAG, "Setting up initial status");
    player.volume = 0;
    player.state = PLAYER_STATE_MENU;
    strcpy(player.track, "N/A");
    strcpy(player.artist, "N/A");
    strcpy(player.album,"N/A");

    // let's initialise the bluetooth service
    bt_service.periph = NULL;
    bt_service.stream = NULL;

    // Let's create the peripherals
    ESP_LOGD(PLAYER_TAG, "Creating peripherals");
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    periph_cfg.task_core = 1;
    player.periph_set = esp_periph_set_init(&periph_cfg);

    // Set up the display
    ESP_LOGD(PLAYER_TAG, "Initializing the display");
    esp_periph_handle_t display_handle = periph_display_init();

    // Set up the buttons
    ESP_LOGD(PLAYER_TAG, "Initializing the buttons");
    periph_button_cfg_t btn_cfg = {
        .gpio_mask = INPUT_GPIO_PIN_MASK,
        .long_press_time_ms = INPUT_PRESS_TIME,
    };
    esp_periph_handle_t btn_handle = periph_button_init(&btn_cfg);
    
    // Set up the audio pipeline
    ESP_LOGD(PLAYER_TAG, "Setup the audio pipeline");
    audio_pipeline_cfg_t pipeline_config = DEFAULT_AUDIO_PIPELINE_CONFIG();
    player.pipeline = audio_pipeline_init(&pipeline_config);
    ESP_ERROR_CHECK(audio_pipeline_set_listener(player.pipeline, player.evt));

    // Add an i2s stream writer
    ESP_LOGD(PLAYER_TAG, "Initialising an I2S stream writer for internal DAC");

    //i2s_stream_cfg_t i2s_stream_config = I2S_STREAM_INTERNAL_DAC_CFG_DEFAULT();
    i2s_stream_cfg_t i2s_stream_config = I2S_STREAM_TX_PDM_CFG_DEFAULT();
    i2s_stream_config.type = AUDIO_STREAM_WRITER;
    i2s_stream_config.use_alc = true;
    i2s_stream_config.task_core = 1;
    i2s_stream_config.i2s_port = I2S_PORT;

    // Let's tweak a little bit the PDM
    i2s_stream_config.i2s_config.use_apll = false;

    audio_element_handle_t i2s_stream_writer = i2s_stream_init(&i2s_stream_config);
    AUDIO_MEM_CHECK(PLAYER_TAG, i2s_stream_writer, return ESP_FAIL);

    ESP_ERROR_CHECK(audio_pipeline_register(player.pipeline, i2s_stream_writer, "i2s_writer"));

    // Set up the bluetooth controller
    ESP_LOGD(PLAYER_TAG, "Initializing the BT service");
    ESP_ERROR_CHECK(bluetooth_service_init(&bt_service));

    // Let's register the event callback
    ESP_LOGD(PLAYER_TAG, "Registering the event callback");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    player.evt = audio_event_iface_init(&evt_cfg);

    // Let's register the peripherals to the event listener
    ESP_LOGD(PLAYER_TAG, "Registering peripherals to the event listener");
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(player.periph_set), player.evt);

    // Start all the peripherals
    ESP_LOGD(PLAYER_TAG, "Starting peripherals");
    esp_periph_start(player.periph_set, display_handle);
    esp_periph_start(player.periph_set, btn_handle);
    esp_periph_start(player.periph_set, bt_service.periph);

    // Let's display a message on the display
    dpl_msg_t *msg = malloc(sizeof(dpl_msg_t));
    msg->id = DISPLAY_ONELINE;
    msg->params.one_line.str = CONFIG_BLUETOOTH_DEVICE_NAME;

    ESP_LOGD(PLAYER_TAG, "Sending ONELINE event to the display");
    ESP_ERROR_CHECK(esp_periph_send_event(display_handle, PLAYER_EVENT_MESSAGE, msg, sizeof(dpl_msg_t)));

    // Let's start the BT stack
    ESP_LOGD(PLAYER_TAG, "Sending signal BT_SIGNAL_INIT");
    BT_SIGNAL(bt_service.periph, BT_SIGNAL_INIT);
}
