#include "string.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_log.h"

#include "u8g2.h"

#include "display.h"
#include "hal.h"
#include "input.h"
#include "player.h"

#include "esp_peripherals.h"
#include "audio_mem.h"

#define GLYPH_PLAY 0x45
#define GLYPH_PAUSE 0x44
#define GLYPH_STOP 0x4B

u8g2_t u8g2;

static void display_msg_handler(dpl_msg_t *msg)
{
    u8g2_FirstPage(&u8g2);
    do {
        switch(msg->id) {
            case DISPLAY_ONELINE:
                // ONE LINE message
                ESP_LOGD(DISPLAY_TAG, "DISPLAY_ONELINE: %s", msg->params.one_line.str);
                u8g2_uint_t width = u8g2_GetUTF8Width(&u8g2, msg->params.one_line.str);
                u8g2_DrawUTF8(&u8g2, (128 - width) / 2, 16, msg->params.one_line.str);
                break;
            case DISPLAY_NOPLAY:
                // NOw PLAYing
                ESP_LOGD(DISPLAY_TAG, "DISPLAY_NOPLAY");
                // This is the play/pause/stop icon
                u8g2_SetFont(&u8g2, u8g2_font_open_iconic_play_2x_t);
                switch(player.state) {
                    case PLAYER_STATE_PAUSE:
                        u8g2_DrawGlyph(&u8g2, 1, 16, GLYPH_PAUSE);
                        break;
                    case PLAYER_STATE_STOP:
                        u8g2_DrawGlyph(&u8g2, 1, 16, GLYPH_STOP);
                        break;
                    case PLAYER_STATE_PLAY:
                        u8g2_DrawGlyph(&u8g2, 1, 16, GLYPH_PLAY);
                        break;
                    default:
                        // Should not be displaying anything in this state if the player is in the
                        // menu state
                        break;
                }

                // We're now going to print track / album / artist on one line each
                // and display volume % and play/pause status
                u8g2_SetFont(&u8g2, u8g2_font_oskool_tf);
                u8g2_DrawUTF8(&u8g2, 20, 5, player.artist);
                u8g2_DrawUTF8(&u8g2, 20, 16, player.album);
                u8g2_DrawUTF8(&u8g2, 20, 27, player.track);

                break;
            case DISPLAY_YESNO:
                ESP_LOGD(DISPLAY_TAG, "DISPLAY_YESNO: %s", msg->params.yes_no.str[0]);
                int display_width = (int)u8g2_GetDisplayWidth(&u8g2);
                for (int i = 0; i <= 1; i++) {
                    int str_width = (int)u8g2_GetStrWidth(&u8g2, msg->params.yes_no.str[i]);
                    // Difference between display and str width / 2 give me the margin
                    int margin = (int)((display_width - str_width) / 2);
                    u8g2_DrawUTF8(&u8g2, margin, 8*(i+1), msg->params.yes_no.str[i]);
                }

                u8g2_DrawHLine(&u8g2, 0, 22, display_width+1);
                char *yesno = "YES    NO";
                int yesno_width = (int)u8g2_GetStrWidth(&u8g2, yesno);
                int margin = (int)((display_width - yesno_width) / 2);
                u8g2_DrawUTF8(&u8g2, margin, 27, yesno);
                break;
            default:
                ESP_LOGW(DISPLAY_TAG, "Display received unknown message: %d", msg->id);
                break;
        }
        u8g2_UpdateDisplay(&u8g2);
    } while (u8g2_NextPage(&u8g2));
}

void display_powersave(uint8_t is_enable)
{
    u8g2_SetPowerSave(&u8g2, is_enable);
}

// Manage the peripheral (init / run / destroy)
esp_err_t _display_destroy(esp_periph_handle_t self)
{
    return ESP_OK;
}

esp_err_t _display_run(esp_periph_handle_t self, audio_event_iface_msg_t *msg)
{
    int ret = ESP_OK;
    if (msg->cmd == PLAYER_EVENT_MESSAGE) {
        dpl_msg_t *dpl_msg = msg->data;
        ESP_LOGI(DISPLAY_TAG, "Received a message of type: %d", dpl_msg->id);
        display_msg_handler(dpl_msg);
    }

    return ret;
}

esp_err_t _display_init(esp_periph_handle_t self)
{
    u8g2_Setup_ssd1306_i2c_128x32_univision_f(&u8g2,
            U8G2_R0,
            u8g2_esp32_i2c_byte_cb,
            u8g2_esp32_gpio_and_delay_cb);
    u8g2_InitDisplay(&u8g2);
    u8g2_SetPowerSave(&u8g2, 0);

    // oskool is a good enough font, let's use it
    u8g2_SetFont(&u8g2, u8g2_font_oskool_tf);
    u8g2_SetFontMode(&u8g2, 0);
    u8g2_SetDrawColor(&u8g2, 1);
    u8g2_SetFontPosCenter(&u8g2);
    u8g2_SetFontRefHeightAll(&u8g2);

    return ESP_OK;
}

esp_periph_handle_t periph_display_init()
{
    // This functio is used to create a peripheral for the ADF framework
    esp_periph_handle_t periph = esp_periph_create(PERIPH_ID_CONSOLE, "periph_dpl");
    AUDIO_MEM_CHECK(DISPLAY_TAG, periph, return NULL);

    ESP_ERROR_CHECK(esp_periph_set_function(
                periph,
                _display_init,
                _display_run,
                _display_destroy));

    return periph;
}
