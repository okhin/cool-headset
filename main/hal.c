#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"

#include "hal.h"
#include "input.h"

#define HAL_TAG "HAL"
#define I2C_TIMEOUT_MS 1000

static i2c_cmd_handle_t hal_i2c_handle;
u8g2_t u8g2;

// HAL callback function to handle i2c
uint8_t u8g2_esp32_i2c_byte_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
	ESP_LOGD(HAL_TAG, "Received a msg for u8g2|i2c: %d", msg);
    switch(msg) {
        case U8X8_MSG_BYTE_INIT:
            i2c_config.mode = I2C_MODE_MASTER;
            i2c_config.sda_io_num = I2C_SDA;
            i2c_config.sda_pullup_en = GPIO_PULLUP_ENABLE;
            i2c_config.scl_io_num = I2C_SCL;
            i2c_config.scl_pullup_en = GPIO_PULLUP_ENABLE;
            i2c_config.master.clk_speed = I2C_MASTER_FREQ_HZ;

            ESP_ERROR_CHECK(i2c_param_config(I2C_MASTER_NUM, &i2c_config));
            ESP_ERROR_CHECK(i2c_driver_install(I2C_MASTER_NUM, i2c_config.mode,
                        I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0));

            break;
        case U8X8_MSG_BYTE_SEND: {
                uint8_t *data_ptr = (uint8_t *)arg_ptr;
                while (arg_int > 0) {
                    ESP_ERROR_CHECK(i2c_master_write_byte(hal_i2c_handle, *data_ptr, ACK_CHECK_EN));
                    data_ptr++;
                    arg_int--;
                }
                break;
            }
        case U8X8_MSG_BYTE_START_TRANSFER:
            hal_i2c_handle = i2c_cmd_link_create();
            ESP_ERROR_CHECK(i2c_master_start(hal_i2c_handle));
            ESP_ERROR_CHECK(i2c_master_write_byte(hal_i2c_handle,
                        I2C_ADDRESS | I2C_MASTER_WRITE, ACK_CHECK_EN));
            break;
           
        case U8X8_MSG_BYTE_END_TRANSFER:
            ESP_ERROR_CHECK(i2c_master_stop(hal_i2c_handle));
            ESP_ERROR_CHECK(i2c_master_cmd_begin(I2C_MASTER_NUM, hal_i2c_handle, I2C_TIMEOUT_MS / portTICK_RATE_MS));
            i2c_cmd_link_delete(hal_i2c_handle);
            break;
        default:
            break;
    }
    return 0;
}

uint8_t u8g2_esp32_gpio_and_delay_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
    switch(msg) {
        case U8X8_MSG_DELAY_MILLI:
            vTaskDelay(arg_int/portTICK_PERIOD_MS);
            break;
        case U8X8_MSG_GPIO_MENU_PREV:
            u8x8_SetGPIOResult(u8x8,
                    gpio_get_level(INPUT_GPIO_0));
            break;
        case U8X8_MSG_GPIO_MENU_SELECT:
            u8x8_SetGPIOResult(u8x8,
                    gpio_get_level(INPUT_GPIO_1));
            break;
        case U8X8_MSG_GPIO_MENU_NEXT:
            u8x8_SetGPIOResult(u8x8,
                    gpio_get_level(INPUT_GPIO_2));
            break;
        default:
            break;
    }
    return 0;
}
