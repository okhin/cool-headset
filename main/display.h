#ifndef __DISPLAY_H
#define __DISPLAY_H

#define DISPLAY_TAG "DISPLAY"

#include "esp_peripherals.h"

#include "u8g2.h"

void display_powersave(uint8_t is_enable);

esp_periph_handle_t periph_display_init();

typedef enum {
    DISPLAY_NOPLAY = 0x0,
    DISPLAY_YESNO,
    DISPLAY_ONELINE,
} dpl_msg_type_t;

typedef struct dpl_yesno {
    char *str[2];
    void (*yes_cb)(void*);
    void (*no_cb)(void*);
    size_t cb_param_size;
    void *cb_param;
} dpl_yesno_t;

typedef struct dpl_oneline {
    char *str;
} dpl_oneline_t;

typedef union dpl_msg_params {
    dpl_yesno_t yes_no;
    dpl_oneline_t one_line;
} dpl_msg_params_t;

typedef struct dpl_msg {
    dpl_msg_type_t id;
    dpl_msg_params_t params;
} dpl_msg_t;
#endif
