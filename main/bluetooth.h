#ifndef __BLUETOOTH_H
#define __BLUETOOTH_H

#include "esp_gap_bt_api.h"
#include "esp_a2dp_api.h"
#include "esp_avrc_api.h"
#include "esp_peripherals.h"

#include "freertos/FreeRTOS.h"
#include "freertos/FreeRTOSConfig.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "audio_error.h"
#include "audio_element.h"
#include "audio_hal.h"

#define BT_PRIORITY (configMAX_PRIORITIES - 1)
#define BT_TAG "BT"
#define BT_SIGNAL(PERIPH, SIGNAL)   (esp_periph_send_cmd(PERIPH, SIGNAL, NULL, 0))
#define BT_EVENT(PERIPH, EVENT)   (esp_periph_send_event(PERIPH, EVENT, NULL, 0))

#define BT_SERVICE_CLASS 0x2C // Bit 19 Capturing | Bit 21 Audio
#define BT_MAJOR_CLASS   0x04 // Audio Video
#define BT_MINOR_CLASS   0x06 // Headphones

typedef enum bt_signal {
    BT_SIGNAL_INIT = 0x0,
    // Connection states
    BT_SIGNAL_CONNECTED,
    BT_SIGNAL_DISCONNECTED,
    BT_SIGNAL_DEEPSLEEP,
    BT_SIGNAL_LIGHTSLEEP,
    // Audio states
    BT_SIGNAL_AUDIO_STARTED,
    BT_SIGNAL_AUDIO_SUSPENDED,
    BT_SIGNAL_AUDIO_STOPPED,
    // Volume states
    BT_AVRC_TG_VOLUME_CHANGED,
    BT_SIGNAL_MAX
} bt_signal_t;

typedef enum bt_state {
    BT_STATE_SHUTDOWN = 0x0,
    BT_STATE_STARTED,
    BT_STATE_DISCOVERABLE,
    BT_STATE_CONNECTED,
    BT_STATE_MAX
} bt_state_t;

const char *s_a2d_audio_state_str[3];
const char *s_a2d_conn_state_str[4];

typedef struct s_bt_service {
    esp_periph_handle_t periph;
    audio_element_handle_t stream;
    bt_state_t state;
} s_bt_service_t;

s_bt_service_t bt_service;
esp_err_t bluetooth_service_init();

#endif // __BLUETOOTH_H
