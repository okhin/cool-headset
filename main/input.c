#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_timer.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "driver/gpio.h"
#include "driver/rtc_io.h"

#include "input.h"
#include "player.h"

static uint32_t input_gpio_pin[3] = {15, 32, 14};

static s_input_t inputs[INPUT_NUM];
static gpio_config_t input_gpio_config;

static void input_handler(s_input_t *input)
{
    int64_t now = esp_timer_get_time();
    if (input->level == 1) // We pushed the button
    {
        int64_t diff = (now - input->timer) / 1000; // In ms not us
        switch (input->gpio) {
            case INPUT_GPIO_0:
                // That's the prev/voldown button
                if (diff < INPUT_PRESS_TIME) {
                    ESP_LOGD(INPUT_TAG, "Volume down");
                    ESP_ERROR_CHECK(esp_event_post_to(player_event_loop,
                                PLAYER_EVENTS, PLAYER_EVENT_VOLDN_SEND,
                                NULL, 0, portMAX_DELAY));
                } else {
                    ESP_LOGD(INPUT_TAG, "Previous track");
                    ESP_ERROR_CHECK(esp_event_post_to(player_event_loop,
                                PLAYER_EVENTS, PLAYER_EVENT_PREV_SEND,
                                NULL, 0, portMAX_DELAY));
                }
                break;
            case INPUT_GPIO_1:
                // That's the play/pause one and deepsleep on/off
                if (diff < 4 * INPUT_PRESS_TIME ) {
                    ESP_LOGD(INPUT_TAG, "Toggle play/pause");
                    ESP_ERROR_CHECK(esp_event_post_to(player_event_loop,
                                PLAYER_EVENTS, PLAYER_EVENT_TOGGLE_SEND,
                                NULL, 0, portMAX_DELAY));
                } else {
                    ESP_LOGD(INPUT_TAG, "Going into sleep");
                    ESP_ERROR_CHECK(esp_event_post_to(player_event_loop,
                                PLAYER_EVENTS, PLAYER_EVENT_DEEPSLEEP,
                                NULL, 0, portMAX_DELAY));
                }
                break;
            case INPUT_GPIO_2:
                // that's the next/volup button
                if (diff < INPUT_PRESS_TIME) {
                    ESP_LOGD(INPUT_TAG, "Volume up");
                    ESP_ERROR_CHECK(esp_event_post_to(player_event_loop,
                                PLAYER_EVENTS, PLAYER_EVENT_VOLUP_SEND,
                                NULL, 0, portMAX_DELAY));
                } else {
                    ESP_LOGD(INPUT_TAG, "Next track");
                    ESP_ERROR_CHECK(esp_event_post_to(player_event_loop,
                                PLAYER_EVENTS, PLAYER_EVENT_NEXT_SEND,
                                NULL, 0, portMAX_DELAY));
                }
                break;
            default:
                ESP_LOGW(INPUT_TAG, "Unhandled release [%d]", input->gpio);
                break;
        }
    }
    input->timer = now;
    input->level = input->level == 1 ? 0 : 1;
}

static void IRAM_ATTR gpio_isr_handler(void *arg)
{
    uint32_t gpio_num = (uint32_t)arg;
    if (pdTRUE == xSemaphoreTakeFromISR(input_smphr, NULL)) {
        xQueueSendFromISR(input_queue_handle, &gpio_num, NULL);
        xSemaphoreGiveFromISR(input_smphr, NULL);
    }
}

static void input_task_handler(void *arg)
{
    uint32_t gpio_num = 0;
    for (;;) {
        if(xQueueReceive(input_queue_handle, &gpio_num, portMAX_DELAY)) {
            for(int i = 0; i < INPUT_NUM; i++) {
                if (inputs[i].gpio == gpio_num) {
                    inputs[i].level = gpio_get_level(gpio_num);
                    ESP_LOGD(INPUT_TAG, "GPIO[%d] LEVEL[%d]",
                            inputs[i].gpio, inputs[i].level);
                    input_handler(&(inputs[i]));
                    break;
                }
            }
        }
    }
}

void input_task_startup()
{
    // Now we create a queue, a semaphore and a task
    input_queue_handle = xQueueCreate(10, sizeof(uint32_t));
    input_smphr = xSemaphoreCreateMutex();
    xTaskCreatePinnedToCore(input_task_handler, "input_task", 2048, NULL, 0, &input_task_handle, 0);

    // Let's get the gpio config
    input_gpio_config.intr_type = GPIO_INTR_ANYEDGE; // Interrupt on both edges
    input_gpio_config.pin_bit_mask = INPUT_GPIO_PIN_MASK;
    input_gpio_config.mode = GPIO_MODE_INPUT;

    // let's push the configuration and start the service
    ESP_ERROR_CHECK(gpio_config(&input_gpio_config));
    ESP_ERROR_CHECK(gpio_install_isr_service(ESP_INTR_FLAG_LEVEL1));

    // Let's finish the inputs init
    for (int i = 0; i < INPUT_NUM; i++) {
        // Store a struct per GPIO
        inputs[i].gpio = input_gpio_pin[i];
        inputs[i].timer = esp_timer_get_time();
        inputs[i].level = 0;
        
        // We have pad pullups, and pad pullups only
        ESP_ERROR_CHECK(gpio_set_pull_mode(input_gpio_pin[i], GPIO_PULLUP_ONLY));

        // We need to register ISR handler
        ESP_ERROR_CHECK(gpio_isr_handler_add(input_gpio_pin[i],
                    gpio_isr_handler,
                    (void *)(input_gpio_pin[i])));

    }
    // Only the play/pause button wakes up from sleep, and it brings the state to low
    ESP_ERROR_CHECK(esp_sleep_enable_ext0_wakeup(INPUT_GPIO_1, 0));
}
