#ifndef __INPUT_H
#define __INPUT_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#define INPUT_TAG   "INPUT"

#define INPUT_GPIO_0 15
#define INPUT_GPIO_1 32
#define INPUT_GPIO_2 14

#define INPUT_GPIO_PIN_MASK ((1ULL << INPUT_GPIO_0) | \
        (1ULL << INPUT_GPIO_1) | \
        (1ULL << INPUT_GPIO_2))

#define INPUT_NUM   3

#define INPUT_PRESS_TIME    CONFIG_PRESS_TIME

typedef struct s_input {
    uint16_t gpio;   // The gpio pin
    int64_t timer;  // Timer at last edge
    uint16_t level; // 0 or 1 for gpios buttons
} s_input_t;

xQueueHandle input_queue_handle;
xTaskHandle input_task_handle;
xSemaphoreHandle input_smphr;

void input_task_startup();

#endif // __INPUT_H
