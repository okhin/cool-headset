#include "esp_sleep.h"
#include "driver/rtc_io.h"
#include "esp_timer.h"

#include "input.h"

void RTC_IRAM_ATTR esp_wake_deep_sleep(void)
{
    esp_default_wake_deep_sleep();
    ESP_ERROR_CHECK(rtc_gpio_init(INPUT_GPIO_1));
    ESP_ERROR_CHECK(rtc_gpio_pullup_en(INPUT_GPIO_1));
    uint32_t level = rtc_gpio_get_level(INPUT_GPIO_1);
    uint64_t time_boot = esp_timer_get_time();

    while (level == 0) { // Button pressed
        level = rtc_gpio_get_level(INPUT_GPIO_1);
        uint64_t time_diff = (esp_timer_get_time() - time_boot) / 1000; // in ms
        if (time_diff > 4000) {
            return;
        }
    }
    // The button has been released
    esp_deep_sleep_start();
}
