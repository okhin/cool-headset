#include "esp_log.h"
#include "audio_element.h"

// application include
#include "player.h"
#include "bluetooth.h"
#include "display.h"

void app_main()
{
    esp_log_level_set("*", ESP_LOG_WARN);
    esp_log_level_set(PLAYER_TAG, ESP_LOG_DEBUG);
    esp_log_level_set(BT_TAG, ESP_LOG_DEBUG);
    esp_log_level_set(DISPLAY_TAG, ESP_LOG_DEBUG);
    ESP_LOGD("MAIN", "Starting the player");
    player_init();

    TaskHandle_t player_task_handle = NULL;

    xTaskCreate(player_task, "PLAYER", 4096, NULL, tskIDLE_PRIORITY, &player_task_handle);
    configASSERT(player_task_handle);
}
