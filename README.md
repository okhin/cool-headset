# Cool Headphones

I'm in the process of 3d printing a headset to replace the broken one I have.
And since I'v spent a lot of time on it, I decided that I probably also could
write a firmware for such a headphone, this is the result of it.

I'm not an embedded software developer, so the code is probably not as sleek
as it could be, but it works sufficiently enough.

I'm using an [ESP32](https://docs.espressif.com/projects/esp-idf/en/release-v4.1/index.html)
and while I could code it in Arduino, Arduino libs for this chip are far from
complete. So, I'm using espressif IDF to code and build this firmware.

## Installing
Setup of the ESP-IDF is well [documented](https://docs.espressif.com/projects/esp-idf/en/release-v4.1/get-started/index.html)
and once you're setup, you can run the following command from the root of a
clone of this repository.

```
make flash monitor
```

There's some default configuration setup, if you want to change it, you'll need
to run before hand.

```
make menuconfig
```